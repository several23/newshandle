# -*- coding: utf-8 -*-
"""
Created on Sat Jun  1 12:23:26 2019

@author: lyrdo
"""

          
from deeppavlov import configs, build_model, train_model

ner_model = build_model(configs.ner.ner_rus, download=True)   
#%%
wer = ner_model(['семейное право России'])
#%%      
import pickle 
#%% load path_name mappring
with open('centrner100geo.pickle', 'rb') as f2:
     name_path_dict5 = pickle.load(f2)
#%%
text_arr =[]
for i, (key, value) in enumerate(name_path_dict.items()):
     text_arr.append(tuple((name_path_dict[i].get('text').lstrip(),name_path_dict[i].get('date'),name_path_dict[i].get('source'))))
name_path_dict ={}
#%%  
with open('../ner/justtext.pickle', 'rb') as f2:
     daters = pickle.load(f2)
#%%
daters=[]     
#%%  
with open('../ner/regional.pickle', 'rb') as f2:
     daters = pickle.load(f2)     
#%%
import pandas as pd     
text_dataframe = pd.DataFrame(daters, columns=['text','date','source'])     
#%%
def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]
if len([text_dataframe.at[6,'text']])//512>1:
    print(chunks([text_dataframe.at[6,'text']], len([text_dataframe.at[6,'text']])//512))
else:
    print([text_dataframe.at[6,'text']][:300])
     
#%%     
name_path_view=name_path_dict[0:20000]
#%%
text_arr=[]
for elem in name_path_dict:
    
          text_arr.append(tuple((elem[3],elem[1],elem[2],elem[0])))
name_path_dict=[]
#%%
for i, (key, value) in enumerate(name_path_dict.items()):
    
          text_arr.append(tuple((name_path_dict[i].get('text'),name_path_dict[i].get('date'),name_path_dict[i].get('source'))))
name_path_dict=[]
#%%
with open('lgihtner.pickle', 'wb') as f:
     data = pickle.dump(text_arr[0:30000],f) 
#%%
text_arr =text_arr[0:1000]
#%%
import pandas as pd
import chardet

text_dataframe = pd.DataFrame(text_arr, columns=['text','month','year','source'])
text_dataframe['encode'] =''
for index, row in  text_dataframe[0:10].iterrows():
    text_dataframe.at[index,'encode'] = chardet.detect('text')['encoding']

#%%
from tqdm import tqdm
text_dataframe=  text_dataframe        
ner_array=[]
with tqdm(total = len(text_dataframe)) as pbar:

    for index,row in text_dataframe.iterrows():
        pbar.update(1)
        ner_array.append(tuple((ner_model([text_dataframe.at[index,'text'][:500]]),text_dataframe.at[index,'month'],text_dataframe.at[index,'year'],text_dataframe.at[index,'source'])))
with open('dataframe.pickle', 'wb') as f:
     data = pickle.dump(ner_array,f)     
#%%
import pickle
with open('../ner/dataframe.pickle', 'rb') as f:
     datafest = pickle.load(f)     
#%%
elemnew=[]
person=''
person_arr=[]
org_arr=[]
org=''
from deeppavlov import configs, build_model, train_model

sent_model = build_model(configs.classifiers.rusentiment_elmo, download=True)  
#%%
wer = sent_model(['Москва и Стамбул'])
#%%
datafester = datafest[0:4000]
#%%
from deeppavlov import configs, build_model, train_model

sent_model = build_model(configs.classifiers.sentiment_twitter, download=False) 
#%%
wer = sent_model(['Москва и Стамбул'])
#%%#%% regex
import pandas as pd
df4=  pd.read_csv('verbs.csv', sep=';')
verbes = df4['verb'].tolist()

#%% спряжение
import pymorphy2
morph = pymorphy2.MorphAnalyzer()
verbmy_form = []
for word in verbes: 
    
    #word = morph.parse(word)[0].normal_form
    word = morph.parse(word)[0]
    print(word)
    pos = word.tag.POS
     
    CASES = {
            ('past','<Прошедшее время>'):[
                {'masc'}
            ]
        }    
     
    if word.tag.POS in {'INFN','VERB'}:
        for key,val in CASES.items():
            print(key[1])
            for cases in val:
                cases.add(key[0])
                if word.inflect(cases) is not None:    
                    w = word.inflect(cases).word
                    verbmy_form.append(w) 
               
#%% new regular
import re            
import pandas as pd
speechdf=  pd.read_csv('speech.csv', sep=';')
speech = speechdf['speech'].tolist()
speechdirect=re.compile('('+'|'.join(speech)+'){1}')
#%%
sent_arr = {}
person_arr=[]
org_arr=[]
from datetime import datetime

def try_parsing_date(text):
    for fmt in ('%Y-%m-%d', '%Y.%m.%d', '%Y/%m/%d'):
        try:
            return datetime.strptime(text, fmt)
        except ValueError:
            pass
    raise ValueError('no valid date format found')
#%%
import string
translator = str.maketrans('', '', string.punctuation)
#%%
text_arr = [] 
date_arr = []   

for elem in daters:
    
   
 
    date_arr.append(tuple((elem[1],elem[2], elem[3],elem[0])))
datafest = []
import pandas as pd
filtdf = pd.DataFrame(date_arr, columns=['month','year','text','source'])
#%%
datafest = []
            #%%
from gen_topic import topic_custom
#%%
datafest = date_arr
#%%
org_arr=[]
person_arr=[]
my_arr=[]
from tqdm import tqdm
import re

with tqdm(total = len( datafest)) as pbar:

    for elem in datafest:
            pbar.update(1)
            cntper=0
            
            сntorg=0
            person=''
            stringer= ' '.join(elem[0][0][0])
            #sent = sent_model([stringer])[0][0]
            date =try_parsing_date(elem[1])
            cntspeech = len([x for x in re.findall(speechdirect, stringer) if len(x)>40])
     
            if len(elem[0][1]) > 0: 
                for inder, val in enumerate(elem[0][1][0]):
                    for tag in ['PER','ORG','LOC']:
                        if 'B-'+tag ==val:
                                startprinted = True
                                tagvalue = elem[0][0][0][inder]
                                try:
                                    if (elem[0][0][0][inder+1]!='I-'+tag):
                                        my_arr.append(tuple((tagvalue,str(date.day),str(date.month),str(date.year),elem[2],tag.lower(),cntspeech)))
                                except IndexError:
                                    my_arr.append(tuple((tagvalue,str(date.day),str(date.month),str(date.year),elem[2],tag.lower(),cntspeech)))
    
        
        
                        elif 'I-'+tag == val:
                                tagvalue  = tagvalue + ' '+elem[0][0][0][inder]
                                try:
                                    if  elem[0][1][0][inder+1] !='I-'+tag:
                                        my_arr.append(tuple((tagvalue,str(date.day),str(date.month),str(date.year),elem[2],tag.lower(),cntspeech)))
                                except IndexError: 
                                    my_arr.append(tuple((tagvalue,str(date.day),str(date.month),str(date.year),elem[2],tag.lower(),cntspeech)))
    
                         
#%%
datafest   =[]               
       
              
#%%
import pickle                    
with open('arrcnt.pickle', 'rb') as f:
     data = pickle.load(f)
#%%
import pickle                    
with open('alltagdump.pickle', 'wb') as f:
     data = pickle.dump(my_arr,f)
#%%
import pandas as pd
text_dataframe = pd.DataFrame(my_arr, columns=['ent','day','month','year','source','typent','cntspeech'])

data=[]   
#%%
import numpy as np
serw =text_dataframe.groupby(['source','year'])['cntspeech'].mean()
#ser = text_dataframe[0:1000].groupby(['ent','date','source']).transform(np.size)

#%%

text_dataframekp = text_dataframe[(text_dataframe["typent"] == 'per')&(text_dataframe["year"] == '2013')]
#%%
text_dataframekpperson = list(set(text_dataframekp['ent'].values.tolist()))
morph_person = {}
for elem in text_dataframekpperson:
    morph_person[elem.rstrip()] = elem.replace(' ','*')+'POS'+'NOUN'
#%%
with open('regionalperson2013theme.pickle', 'wb') as f:
     datarr = pickle.dump(adsf,f)    
#text_dataframekp = text_dataframe[(text_dataframe["year"] == '2014')]
#%%
serw =text_dataframe.groupby(['source','year'])['cntspeech'].mean()
#%%
serw= text_dataframekp.groupby(['source','month','day'])['ent'].apply(list)
#%%
df =pd.DataFrame(text_dataframekp.groupby(['source','month','day']).agg({'ent':lambda x: list(x)}))
#%%
tupler = []
for index, row in df[1:].iterrows():
    result = [tupler.append(tuple((row['ent'][p1], row['ent'][p2]))) for p1 in range(len(row['ent'])) for p2 in range(p1+1,len(row['ent']))]

#%%
from combinemethods import collect_pairs
tuplecombarry = []
#%%
ent_arr=[]
from tqdm import tqdm 
with tqdm (total = len(df)) as pbar:
    for index,row in df.iterrows():
        pbar.update(1)
        ent_arr.append(row['ent']      )  
        pairs = collect_pairs(row['ent'][:200])
        for element in pairs:
            if len(element[1])>3 and len(element[0])>3:
                tuplecombarry.append(tuple((index,element[0].replace('«','')+' '+element[1].replace('«',''))))
#%%
pair_dataframe = pd.DataFrame(tuplecombarry, columns=['key','pair'])
pair_dataframe['cntent']=pair_dataframe.groupby(['pair']).pair.transform('size')
            
#%%https://stackoverflow.com/questions/21297740/how-to-find-set-of-most-frequently-occurring-word-pairs-in-a-file-using-python
#%%

#df[(df['col1'] >= 1) & (df['col1'] <=1 )]
text_dataframekp['cntent']=text_dataframe.groupby(['ent', 'month','typent','source'])['ent'].transform('size')
text_dataframekp['total']=text_dataframekp['cntent']/text_dataframekp.groupby([ 'month','typent','source'])['cntent'].transform('sum')

ser=text_dataframekp.groupby(['ent', 'year','typent','source','month']).size()
#%%
dfres=text_dataframekp[['ent','month',"year",'source','cntent','total']].drop_duplicates()
#%%
dfres['form'] =''
for index,row in dfres.iterrows():
    dfres.at[index, 'form'] =str(dfres.at[index,'ent']) +' - '+str(dfres.at[index,'cntent']) + ' упоминаний в газете '+ dfres.at[index,'source'] +' что составило ' +str(round(dfres.at[index,'total'], 2)*100)+ '% от общего количества упоминаний за месяц' 

 
#%%
g = dfres.groupby(["month","year","source"]).apply(lambda x: x.sort_values(["cntent"], ascending = False)).reset_index(drop=True)
# select top N rows within each continent
#%%

adsf =g.groupby(["month","year","source"]).head(3).reset_index(drop=True)
#%%
import wikipedia
wikipedia.set_lang("ru")
from tqdm import tqdm
g['shortinfo'] = ''
g['lenent'] = ''

for index, row in g.iterrows():

    g.at[index,'ent'] = g.at[index,'ent'].translate(translator)
    if len(g.at[index,'ent'].split(' ')) < 2:
       g.at[index,'lenent'] = 'no'
    else:
           g.at[index,'lenent'] = 'yes'
       
#%%
g = g[(g['lenent']== 'yes')] 
#%%
adsf['categ'] =''
ny = wikipedia.page("Алла Пугачева")
print(type(ny))
lister = ny.categories
#%%
adsf['shortinfo']=''
with tqdm(total = len( adsf)) as pbar:

    for index, row in adsf.iterrows():
        pbar.update(1)
    
        try:
            wikivar =  wikipedia.page(row['ent'])
           
                
            adsf.at[index, 'shortinfo'] = wikivar.categories                
        except  wikipedia.exceptions.PageError: 
            adsf.at[index, 'shortinfo'] = 'not found'
        except  wikipedia.exceptions.DisambiguationError: 
            adsf.at[index, 'shortinfo'] = 'multy'        
#print (wikipedia.summary("РБК Daily",sentences=1))
#print(wikipedia.search("РБК", results=3))
#%%
adsf['actual_categ'] = ''
for index, row in adsf.iterrows()     :
    for elem in adsf.at[index, 'shortinfo'] :
        settted = False
        if not settted:
            if elem !='':
                
                ner_elem = ner_model([elem])
                if len(ner_elem[1]) > 0:
                    if 'B-LOC' in ner_elem[1][0] and  elem!='Категория:Статьи со ссылками на Викиновости':
                        print(elem)
                        adsf.at[index, 'shortinfo']= elem
                        settted = True
#%%
                    
#%%
orger =adsf.groupby([ 'month','shortinfo','source'])['cntent'].transform('sum')        
#%%
from natasha import (
    NamesExtractor,
    LocationExtractor,
    MoneyExtractor,
)
extractor = LocationExtractor()
matches = extractor('Венеция')
spans = [_.span for _ in matches]
facts = [_.fact.as_json for _ in matches]                

#%%
import pickle
with open('wikidfwithcateg.pickle', 'rb') as f:
     data = pickle.load(f)  
#%%        
import pickle
with open('morph1000geo.pickle', 'rb') as f:
     data = pickle.load(f)       
#%%
from pandas import ExcelWriter

writer = ExcelWriter('res2014.xlsx')
adsf.to_excel(writer,'Sheet5')
writer.save()
#%%
g.groupby(["date","ent","source"]).head(1)
#%%
with open('../ner/dataframetext.pickle', 'rb') as f:
     data = pickle.load(f)  
#%%
lister = []
for elem in datafest:
    lister.append(" ".join(elem[0][0][0]))
datafest = []    
#%%    
import pandas as pd
textcorp_dataframe = pd.DataFrame(lister, columns=['text'])
#%%
lister =[]    
#%%
textcorp_dataframe['morphy'] = ''
textcorp_dataframe['posmorphy'] = ''
#%%
#%% preprocessing
from morph_anl import  token_frame
#%%
from morph_anl import  morph_anly     
#%%
est2 = token_frame(textcorp_dataframe, 'text')
print(len(est2))
#%%

def filters (filtedf,month,year,ent, source):
    
    filtedf = filtedf[(filtedf["year"] == year)&(filtedf["month"] == month)&(filtedf["text"].str.contains(ent) == True)&(filtedf["source"] == source)]
    text_arr = filtedf['text'].values.tolist()
    text_arr= [var.translate(translator).replace('«','').replace('»','') for var in text_arr] 
    return text_arr
#%%
#%%

def filtergaz (filtedf,month,year, source):
    
    filtedf = filtedf[(filtedf["year"] == year)&(filtedf["month"] == month)&(filtedf["source"] == source)]
    text_arr = filtedf['text'].values.tolist()
    text_arr= [var.translate(translator).replace('«','').replace('»','') for var in text_arr] 
    return text_arr    
#%%
g = adsf.groupby(["month","source"]).apply(lambda x: x.sort_values(["cntent"], ascending = False)).reset_index(drop=True)
#%%
adsf =g.groupby(["month","source"]).head(5).reset_index(drop=True)
    
#%%
from tqdm import tqdm
adsf['theme'] = ''
with tqdm(total = len( adsf)) as pbar:

    for index,row in adsf.iterrows():
        pbar.update(1)
    
        adsf.at[index,'theme'] = topic_custom(filters(filtdf,row['month'],row['year'],row['ent'],row['source']))
        print('done')
#%%
with open('regionalpersonallyeartheme.pickle', 'wb') as f:
     datanews = pickle.dump(adsf,f) 
#%%
data = adsf[['month','source']]
data = data.drop_duplicates()        
#%%
from tqdm import tqdm
data['theme'] = ''
with tqdm(total = len( data)) as pbar:

    for index,row in data.iterrows():
        pbar.update(1)
    
        data.at[index,'theme'] = topic_custom(filtergaz(filtdf,row['month'],'2013',row['source']))
        print('done')
#%%
with open('../ner/topthemeperson2013.pickle', 'rb') as f:
    
    
    
    
     data = pickle.load(f)     
#%%
import pickle     
with open('ner_array.pickle', 'rb') as f:
     datanews = pickle.load(f) 
#%%
datanews['key']= datanews['month']+datanews['source']
#%%
dictnews = pd.Series(datanews.theme.values,index=datanews.key).to_dict()
#%%
adsf['themegeneral']= ''
for index,row in adsf.iterrows():
          adsf.at[index,'themegeneral'] = dictnews[row['month']+row['source']]
#%%
print(est2[2])
#%%
del textcorp_dataframe
#%%
dre2  = morph_anly(est2)

#%%
timedata=pd.read_excel(open('datatime.xlsx','rb'))
#%%
for index, row in data.iterrows():
    timedata.at[index,'Year']= 2013
    timedata.at[index,'Month']= data.at[index,'month']
    timedata.at[index,'Day']= 1
    timedata.at[index,'Headline']= data.at[index,'ent'] +' ' + str(data.at[index,'shortinfo'])
    timedata.at[index,'Text']= data.at[index,'form'] + '\n'+ str(data.at[index,'theme'])
#%%
from pandas import ExcelWriter
writer = ExcelWriter('speechdumpnewregex.xlsx')
timedata.to_excel(writer,'Sheet5')
writer.save()   
#%%
dataviewer = datareg[0:15] 
#%%
dall = {}
regionalarr = []
import pickle
for filename in os.listdir('../ner/regional'):

    with open('../ner/regional/'+filename, 'rb') as f:
         regional = pickle.load(f) 
         regionalarr = regionalarr + regional 
#%%
import os
for filename in os.listdir('morphdict'):
     print(filename)
#%%
from nerarrayhandle import nerhandler     
regioanl_arr =  nerhandler(regionalarr, speechdirect) 
#%%
regional_view = regioanl_arr[0:1000]
#%%
import pandas as pd
text_dataframe = pd.DataFrame(regioanl_arr, columns=['ent','month','year','source','typent','cntspeech'])
#%%
regioanl_arr=[] 