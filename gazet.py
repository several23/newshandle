# -*- coding: utf-8 -*-
"""
Created on Sat Jun  1 12:23:26 2019

@author: lyrdo
"""

          
from deeppavlov import configs, build_model, train_model

ner_model = build_model(configs.ner.ner_rus, download=True)   
#%%
wer = ner_model(['2 килограмма'])
#%%      
import pickle 
#%% load path_name mappring
with open('articles_with_ner.pickle', 'rb') as f2:
     name_path_dict = pickle.load(f2)
#%%     
name_path_view=name_path_dict[0:20000]
#%%
text_arr=[]
for i, (key, value) in enumerate(name_path_dict.items()):
    
          text_arr.append(tuple((name_path_dict[i].get('text'),name_path_dict[i].get('date'),name_path_dict[i].get('source'))))
name_path_dict=[]
#%%
with open('lgihtner.pickle', 'wb') as f:
     data = pickle.dump(text_arr[0:10000],f) 
#%%
text_arr =text_arr[0:1000]
#%%
import pandas as pd
text_dataframe = pd.DataFrame(text_arr, columns=['text','date','source'])
#%%
from tqdm import tqdm
text_dataframe=  text_dataframe        
ner_array=[]
with tqdm(total = len(text_dataframe)) as pbar:

    for index,row in text_dataframe.iterrows():
        pbar.update(1)
        ner_array.append(tuple((ner_model([text_dataframe.at[index,'text']]),text_dataframe.at[index,'date'],text_dataframe.at[index,'source'])))
with open('dataframe.pickle', 'wb') as f:
     data = pickle.dump(ner_array,f)     
#%%
import pickle
with open('dataframe.pickle', 'rb') as f:
     datafest = pickle.load(f)     
#%%
elemnew=[]
person=''
person_arr=[]
org_arr=[]
org=''
from deeppavlov import configs, build_model, train_model

sent_model = build_model(configs.classifiers.rusentiment_bert, download=False)  
#%%
wer = sent_model(['Москва и Стамбул'])
#%%
datafest = datafest[0:40000]
#%%
from deeppavlov import configs, build_model, train_model

sent_model = build_model(configs.classifiers.sentiment_twitter, download=False) 
#%%
wer = sent_model(['Москва и Стамбул'])
#%%#%% regex
import pandas as pd
df4=  pd.read_csv('verbs.csv', sep=';')
verbes = df4['verb'].tolist()

#%% спряжение
import pymorphy2
morph = pymorphy2.MorphAnalyzer()
verbmy_form = []
for word in verbes: 
    
    #word = morph.parse(word)[0].normal_form
    word = morph.parse(word)[0]
    print(word)
    pos = word.tag.POS
     
    CASES = {
            ('past','<Прошедшее время>'):[
                {'masc'}
            ]
        }    
     
    if word.tag.POS in {'INFN','VERB'}:
        for key,val in CASES.items():
            print(key[1])
            for cases in val:
                cases.add(key[0])
                if word.inflect(cases) is not None:    
                    w = word.inflect(cases).word
                    verbmy_form.append(w) 
               
#%% new regular
import re            
import pandas as pd
speechdf=  pd.read_csv('speech.csv', sep=';')
speech = speechdf['speech'].tolist()
speechdirect=re.compile('('+'|'.join(speech)+'){1}')
#%%
sent_arr = {}
person_arr=[]
org_arr=[]
from datetime import datetime

def try_parsing_date(text):
    for fmt in ('%Y-%m-%d', '%Y.%m.%d', '%Y/%m/%d'):
        try:
            return datetime.strptime(text, fmt)
        except ValueError:
            pass
    raise ValueError('no valid date format found')
#%%
import string
translator = str.maketrans('', '', string.punctuation)
#%%
text_arr = [] 
date_arr = []   

for elem in datafest:
    
    stringel = ' '.join(elem[0][0][0])
 
    date_arr.append(tuple((str(try_parsing_date(elem[1]).month),str(try_parsing_date(elem[1]).year), stringel,elem[2])))
datafest = []
filtdf = pd.DataFrame(date_arr, columns=['month','year','text','source'])
            #%%
from gen_topic import topic_custom
#%%

#%%
org_arr=[]
person_arr=[]
my_arr=[]
from tqdm import tqdm
import re

with tqdm(total = len( datafest)) as pbar:

    for elem in datafest:
        pbar.update(1)
        cntper=0
        
        сntorg=0
        person=''
        stringer= ' '.join(elem[0][0][0])
        #sent = sent_model([stringer])[0][0]
        date =try_parsing_date(elem[1])
        cntspeech = len([x for x in re.findall(speechdirect, stringer) if len(x)>40])
     
        try: 
            for inder, val in enumerate(elem[0][1][0]):
                if 'B-PER' ==val:
                    if inder>len(elem[0][1][0])-2:                        
                        my_arr.append(tuple((elem[0][0][0][inder],str(date.month),str(date.year),elem[2],'per',cntspeech)))
        
                    else:
                        if elem[0][1][0][inder+1]=='I-PER':
                            my_arr.append(tuple((elem[0][0][0][inder]+' '+elem[0][0][0][inder+1],str(date.month),str(date.year),elem[2],'per',cntspeech)))
                        
        
                 
                if 'B-ORG' ==val:
                    org=''
                    if inder>len(elem[0][1][0])-2:
                        my_arr.append(tuple((elem[0][0][0][inder],str(date.month),str(date.year),elem[2],'org',cntspeech)))
        
                    else:
                        ren=False
                        mer=0
                        while inder+mer < len(elem[0][1][0])-2 and not ren:
                            mer+=1
                            if elem[0][1][0][inder+mer]=='I-ORG':
                                org = elem[0][0][0][inder]+' '+elem[0][0][0][inder+mer]
                            if elem[0][1][0][inder+mer]=='O':
                                ren=True
                                if org!='':
                                    my_arr.append(tuple((org,str(date.month),str(date.year),elem[2],'org',cntspeech)))       
                if 'B-LOC' ==val:
                    loc=''
                    if inder>len(elem[0][1][0])-2:
                        my_arr.append(tuple((elem[0][0][0][inder],str(date.month),str(date.year),elem[2],'loc',cntspeech)))
        
                    else:
                        ren=False
                        mer=0
                        while inder+mer < len(elem[0][1][0])-2 and not ren:
                            mer+=1
                            if elem[0][1][0][inder+mer]=='I-LOC':
                                org = elem[0][0][0][inder]+' '+elem[0][0][0][inder+mer]
                            if elem[0][1][0][inder+mer]=='O':
                                ren=True
                                if org!='':
                                    my_arr.append(tuple((org,str(date.month)+str(date.year),elem[2],'loc',cntspeech)))    
        except IndexError: 
                    pass
#%%
with open('dataframe2.pickle', 'rb') as f:
     data = pickle.load(f)

#%%
import pandas as pd
text_dataframe = pd.DataFrame(data, columns=['ent','month','year','source','typent','cntspeech'])

data=[]   
#%%
import numpy as np
serw =text_dataframe.groupby(['source','year'])['cntspeech'].mean()
#ser = text_dataframe[0:1000].groupby(['ent','date','source']).transform(np.size)
#%%

#%%
text_dataframekp = text_dataframe[(text_dataframe["year"] == '2013')&(text_dataframe["typent"] == 'per')]

#df[(df['col1'] >= 1) & (df['col1'] <=1 )]
text_dataframekp['cntent']=text_dataframe.groupby(['ent', 'month','typent','source'])['ent'].transform('size')
text_dataframekp['total']=text_dataframekp['cntent']/text_dataframekp.groupby([ 'month','typent','source'])['cntent'].transform('sum')

ser=text_dataframekp.groupby(['ent', 'year','typent','source','month']).size()
#%%
dfres=text_dataframekp[['ent','month','source','cntent','total']].drop_duplicates()
#%%
dfres['form'] =''
for index,row in dfres.iterrows():
    dfres.at[index, 'form'] =str(dfres.at[index,'ent']) +' - '+str(dfres.at[index,'cntent']) + ' упоминаний в газете '+ dfres.at[index,'source'] +' что составило ' +str(round(dfres.at[index,'total'], 2)*100)+ '% от общего количества упоминаний за месяц' 

 
#%%
g = dfres.groupby(["month","source"]).apply(lambda x: x.sort_values(["cntent"], ascending = False)).reset_index(drop=True)
# select top N rows within each continent
#%%

adsf =g.groupby(["month","source"]).head(20).reset_index(drop=True)
#%%
import wikipedia
wikipedia.set_lang("ru")
from tqdm import tqdm
adsf['shortinfo'] = ''
adsf['lenent'] = ''

for index, row in adsf.iterrows():

    adsf.at[index,'ent'] = adsf.at[index,'ent'].translate(translator)
    if len(adsf.at[index,'ent'].split(' ')) !=2:
       adsf.at[index,'lenent'] = 'no'
    else:
       if len(adsf.at[index,'ent'].split(' ')[1]) > 1:
           adsf.at[index,'lenent'] = 'yes'
       else:
           adsf.at[index,'lenent'] = 'no'
#%%
adsf = adsf[(adsf['lenent']== 'yes')] 
#%%
with tqdm(total = len( adsf)) as pbar:

    for index, row in adsf.iterrows():
        pbar.update(1)
    
        try:
            wikivar =  wikipedia.summary(row['ent'])
            if len(wikivar.split('-')) > 1:
                
                adsf.at[index, 'shortinfo'] = wikivar
            else:
                
                adsf.at[index, 'shortinfo'] = wikivar                
        except  wikipedia.exceptions.PageError: 
            adsf.at[index, 'shortinfo'] = 'not found'
        except  wikipedia.exceptions.DisambiguationError: 
            adsf.at[index, 'shortinfo'] = 'multy'        
#print (wikipedia.summary("РБК Daily",sentences=1))
#print(wikipedia.search("РБК", results=3))
#%%
with open('wikidf.pickle', 'wb') as f:
     data = pickle.dump(adsf,f)                
#%%
from pandas import ExcelWriter

writer = ExcelWriter('res2014.xlsx')
adsf.to_excel(writer,'Sheet5')
writer.save()
#%%
g.groupby(["date","ent","source"]).head(1)
#%%
with open('dataframetext.pickle', 'rb') as f:
     data = pickle.load(f)  
#%%
lister = []
for elem in datafest:
    lister.append(" ".join(elem[0][0][0]))
datafest = []    
#%%    
import pandas as pd
textcorp_dataframe = pd.DataFrame(lister, columns=['text'])
#%%
lister =[]    
#%%
textcorp_dataframe['morphy'] = ''
textcorp_dataframe['posmorphy'] = ''
#%%
#%% preprocessing
from morph_anl import  token_frame
#%%
from morph_anl import  morph_anly     
#%%
est2 = token_frame(textcorp_dataframe, 'text')
print(len(est2))
#%%
def filtersgazet (filtedf,month,year, source):
    
    filtedf = filtedf[(filtedf["year"] == year)&(filtedf["month"] == month)&(filtedf["source"] == source)]
    text_arr = filtedf['text'].values.tolist()
    text_arr= [var.translate(translator).replace('«','').replace('»','') for var in text_arr] 
    return text_arr

#%%

def filters (filtedf,month,year,ent, source):
    
    filtedf = filtedf[(filtedf["year"] == year)&(filtedf["month"] == month)&(filtedf["text"].str.contains(ent) == True)&(filtedf["source"] == source)]
    text_arr = filtedf['text'].values.tolist()
    text_arr= [var.translate(translator).replace('«','').replace('»','') for var in text_arr] 
    return text_arr

#%%
adsf['theme'] = ''
with tqdm(total = len( adsf)) as pbar:

    for index,row in adsf.iterrows():
        pbar.update(1)
    
        adsf.at[index,'theme'] = topic_custom(filters(filtdf,row['month'],'2013',row['ent'],row['source']))
        print('done')
#%%        
        
uniqegazet = filtdf
#%%
uniqegazet= uniqegazet.drop_duplicates()
 #%%       
uniqegazet['theme'] = ''
from tqdm import tqdm

with tqdm(total = len( uniqegazet)) as pbar:

    for index,row in uniqegazet.iterrows():
        pbar.update(1)
    
        uniqegazet.at[index,'theme'] = topic_custom(filtersgazet(filtdf,row['month'],'2013',row['source']))
        print('done')
        
#%%
with open('topwiththemes.pickle', 'wb') as f:
     data = pickle.dump(adsf,f)     
#%%
import pickle     
with open('topwiththemes.pickle', 'rb') as f:
     datanews = pickle.load(f)        
#%%
print(est2[2])
#%%
del textcorp_dataframe
#%%
dre2  = morph_anly(est2)