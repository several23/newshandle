# -*- coding: utf-8 -*-
"""
Created on Sat Jun 15 01:54:08 2019

@author: lyrdo
"""

#%%
from gensim.corpora import *
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import pymorphy2
from gensim.models import Phrases
from gensim.models import phrases

def  topic_custom(text_arr):
    if len(text_arr) > 0:
        morph = pymorphy2.MorphAnalyzer()
        mystopwords = stopwords.words('russian') + ['это', 'наш' ,'который', 'тыс', 'млн', 'млрд', 'также',  'т', 'д', '-', '-']
        # Split the documents into tokens.
        tokenizer = RegexpTokenizer(r'\w+')
        
        from gensim import corpora
        
        texts = [[word for word in document.lower().split() if word not in mystopwords] for document in text_arr]
        for texter in texts:
            for elem, i in enumerate(texter):
                texter[elem] = morph.parse(i)[0].normal_form
        bigram = Phrases(texts, min_count=2, threshold=20) # higher threshold fewer phrases.
        trigram = Phrases(bigram[texts], threshold=20)  
        print(texts[0:5])
        #print(text_data[:10])
        bigram_mod = phrases.Phraser(bigram)
        trigram_mod = phrases.Phraser(trigram)
        bigrammed = [bigram_mod[words] for words in texts]
        trigrammed = [trigram_mod[words] for words in texts]
        
        id2word = Dictionary(bigrammed)
        corpusbi = [id2word.doc2bow(text) for text in bigrammed]
        id2wordtri = Dictionary(trigrammed)
        corpustri = [id2wordtri.doc2bow(text) for text in trigrammed]
        
        dictionary = Dictionary(texts)
        #print(text_data)                        
        corpus = [dictionary.doc2bow(text) for text in texts]
    
    
        print(id2wordtri)
        
        
        
        
        import gensim
        lda = gensim.models.ldamodel.LdaModel(corpus=corpustri, id2word=id2wordtri, num_topics=15,
                                alpha='auto', eta='auto', iterations = 20, passes = 10)
        
        
        doc_lda = lda[corpus]
        print(doc_lda)
        
        
        
        
        return lda.show_topics(5)  
    else:
        return ''
           