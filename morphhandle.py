# -*- coding: utf-8 -*-
"""
Created on Tue Jun 25 02:25:20 2019

@author: lyrdo
"""
from rnnmorph.predictor import RNNMorphPredictor
predictor = RNNMorphPredictor(language="ru")
newArray = []
uniqueArray=[]
text_arr=[]
nltk.download('punkt')
import string
translator = str.maketrans('', '', string.punctuation)
import nltk
from nltk.tokenize.toktok import ToktokTokenizer
toktok = ToktokTokenizer()

from tqdm import tqdm
def tok_frame(frame_name)
tokArray = []
with tqdm(total = len( frame_name[0:15000])) as pbar:

        for index, row in text_dataframe[0:15000].iterrows():
            pbar.update(1)

            sent_text = nltk.sent_tokenize(row['text']) # this gives us a list of sentences
            for senten in sent_text:
                sd= senten.translate(translator)    
                tokArray.append(toktok.tokenize(sd, return_str=False))

for elem in tokArray:
    for word in elem:
        uniqueArray.append(word)
uniqueArray  = list(set(uniqueArray))     
forms = predictor.predict([uniqueArray[5]])
tagged = {}

with tqdm(total = len( uniqueArray)) as pbar:

        for inder,elemer in enumerate(uniqueArray):
                pbar.update(1)

    
           
                
                forms = predictor.predict([elemer])
                tagged[elemer] = forms[0].normal_form+'_'+forms[0].pos
                #print(forms.normal_form+'_'+forms.pos)
                if len(tagged) > 50000:
                    with open('morph'+str(inder)+'geo.pickle', 'wb') as f:
                        data = pickle.dump(tagged,f) 
                        tagged = {}