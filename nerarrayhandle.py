# -*- coding: utf-8 -*-
"""
Created on Sun Jun 23 00:39:34 2019

@author: lyrdo
"""

#%%
org_arr=[]
person_arr=[]
my_arr=[]
from tqdm import tqdm
import re
def nerhandler (nerdataname, speechregular):
    with tqdm(total = len( nerdataname)) as pbar:
    
        for elem in nerdataname:
            pbar.update(1)
            cntper=0
            
            сntorg=0
            person=''
            stringer= ' '.join(elem[0][0][0])
            #sent = sent_model([stringer])[0][0]
            cntspeech = len([x for x in re.findall(speechregular, stringer) if len(x)>40])
            if len(elem[0][1]) > 0: 
                for inder, val in enumerate(elem[0][1][0]):
                    for tag in ['PERSON','ORG','DATE','GPE','NORP','QUANTITY','WORK_OF_ART','CARDINAL','MONEY','ORDINAL','TIME','FAC','LAW','PRODUCT','LANGUAGE','EVENT','LOC']:
                        if 'B-'+tag ==val:
                                tagvalue = elem[0][0][0][inder]
                                try:
                                    if (elem[0][1][0][inder+1]!='I-'+tag):
                                        my_arr.append(tuple((tagvalue,elem[1],elem[2],elem[3],tag.lower(),cntspeech)))
                                except IndexError:
                                    my_arr.append(tuple((tagvalue,elem[1],elem[2],elem[3],tag.lower(),cntspeech)))
        
        
                        elif 'I-'+tag == val:
                                tagvalue  = tagvalue + ' '+elem[0][0][0][inder]
                                try:
                                    if  elem[0][1][0][inder+1] !='I-'+tag:
                                        my_arr.append(tuple((tagvalue,elem[1],elem[2],elem[3],tag.lower(),cntspeech)))
                                except IndexError: 
                                    my_arr.append(tuple((tagvalue,elem[1],elem[2],elem[3],tag.lower(),cntspeech)))
            
    return my_arr        